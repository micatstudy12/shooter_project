// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "shooter_projectGameMode.generated.h"

UCLASS(minimalapi)
class Ashooter_projectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Ashooter_projectGameMode();
};



