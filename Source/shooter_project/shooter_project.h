#pragma once

#include "CoreMinimal.h"
#include "Kismet/KIsmetSystemLibrary.h"

// World Item Actor 에 추가되는 태그입니다.
#define TAG_WORLDITEM			FName(TEXT("WorldItem"))


// 총구 위치를 나타내는 소켓 이름
#define SOCKET_NAME_FIRE_POS	FName(TEXT("Socket_Fire"))

// 총알 생성 후 제거까지 걸리는 시간
#define BULLETACTOR_LIFETIME	4.0f

#define WLOG(_log_, ...) UE_LOG(LogTemp, Warning, _log_, ##__VA_ARGS__)

#define PLOG(_log_, ...)													\
UKismetSystemLibrary::PrintString(											\
GetWorld(), FString::Printf(_log_, ##__VA_ARGS__), true, true, FLinearColor::Green, 3.0f);
