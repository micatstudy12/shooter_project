// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/PlayerState/GamePlayerState.h"

AGamePlayerState::AGamePlayerState()
{
	CurrentHp = 100.0f;
}

void AGamePlayerState::SetCurrentHp(float currentHp)
{
	CurrentHp = currentHp;
}

float AGamePlayerState::GetCurrentHp() const
{
	return CurrentHp;
}
