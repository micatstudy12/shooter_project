// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "GamePlayerState.generated.h"

/**
 * 
 */
UCLASS()
class AGamePlayerState : public APlayerState
{
	GENERATED_BODY()


private :
	UPROPERTY()
	float CurrentHp;

public :
	AGamePlayerState();

	void SetCurrentHp(float currentHp);
	float GetCurrentHp() const;
	
};
