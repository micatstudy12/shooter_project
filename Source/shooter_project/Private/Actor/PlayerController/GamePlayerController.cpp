#include "Actor/PlayerController/GamePlayerController.h"
#include "Actor/PlayerCharacter/PlayerCharacter.h"
#include "Widget/PlayerWidget/PlayerWidget.h"

AGamePlayerController::AGamePlayerController()
{
	static ConstructorHelpers::FClassFinder<UPlayerWidget> WIDGETBP_PLAYERWIDGET(
		TEXT("/Script/UMGEditor.WidgetBlueprint'/Game/Blueprints/Widget/WidgetBP_PlayerWidget.WidgetBP_PlayerWidget_C'"));
	if (WIDGETBP_PLAYERWIDGET.Succeeded())
	{
		WidgetBP_PlayerWidget = WIDGETBP_PLAYERWIDGET.Class;
	}

}

void AGamePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &ThisClass::OnFirePressed);
	InputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Released, this, &ThisClass::OnFireReleased);

	InputComponent->BindAction(TEXT("Zoom"), EInputEvent::IE_Pressed, this, &ThisClass::ZoomIn);
	InputComponent->BindAction(TEXT("Zoom"), EInputEvent::IE_Released, this, &ThisClass::ZoomOut);

	InputComponent->BindAction(TEXT("GetItem"), EInputEvent::IE_Pressed, this, &ThisClass::OnGetItemPressed);

	InputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &ThisClass::OnReloadPressed);

	InputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ThisClass::OnJumpInput);
	InputComponent->BindAxis(TEXT("Horizontal"), this, &ThisClass::OnHorizontalInput);
	InputComponent->BindAxis(TEXT("Vertical"), this, &ThisClass::OnVerticalInput);

	InputComponent->BindAxis(TEXT("MouseX"), this, &ThisClass::OnMouseX);
	InputComponent->BindAxis(TEXT("MouseY"), this, &ThisClass::OnMouseY);

}

void AGamePlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (!IsLocalPlayerController()) return;

	// 플레이어 위젯을 생성합니다.
	PlayerWidget = CreateWidget<UPlayerWidget>(this, WidgetBP_PlayerWidget);

	// 플레이어 위젯을 화면에 표시합니다.
	PlayerWidget->AddToPlayerScreen();

	PlayerCameraManager->ViewPitchMin = -50.0f;
	PlayerCameraManager->ViewPitchMax = 50.0f;
}

void AGamePlayerController::OnGetItemPressed()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnGetItemPressed();
}

void AGamePlayerController::OnGetItemReleased()
{
}

void AGamePlayerController::OnFirePressed()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnFirePressed();
}

void AGamePlayerController::OnFireReleased()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnFireReleased();
}

void AGamePlayerController::OnReloadPressed()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnReloadPressed();
}

void AGamePlayerController::OnZoomPressed()
{
}

void AGamePlayerController::OnZoomReleased()
{
}

void AGamePlayerController::ZoomIn()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->ZoomIn();
}

void AGamePlayerController::ZoomOut()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->ZoomOut();
}

void AGamePlayerController::OnJumpInput()
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnJumpInput();
}

void AGamePlayerController::OnVerticalInput(float axis)
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnVerticalInput(axis);

}

void AGamePlayerController::OnHorizontalInput(float axis)
{
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	if (!IsValid(playerCharacter)) return;

	playerCharacter->OnHorizontalInput(axis);
}

void AGamePlayerController::OnMouseX(float axis)
{
	AddYawInput(axis);
}

void AGamePlayerController::OnMouseY(float axis)
{
	AddPitchInput(axis);

	//FRotator controlRotation = GetControlRotation();
	//UE_LOG(LogTemp, Warning, TEXT("rot = %.2f"), controlRotation.Pitch);
}

FAuthType AGamePlayerController::CheckAuthonication(AActor* actor, APlayerController* ownerClient)
{
	if (actor->HasAuthority()) // Is Server
		return IS_SERVER;

	else if (!IsValid(ownerClient))
		return IS_OTHERCLIENT;

	else if (ownerClient == ownerClient->GetWorld()->GetFirstPlayerController())
			return IS_MYCLIENT;

	return IS_OTHERCLIENT;
}
