// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GamePlayerController.generated.h"

typedef int32 FAuthType;
#define IS_SERVER 0 
#define IS_MYCLIENT 1
#define IS_OTHERCLIENT 2


/**
 * 
 */
UCLASS()
class AGamePlayerController : public APlayerController
{
	GENERATED_BODY()

private :
	// ȭ�鿡 ��� �÷��̾� ���� Ŭ����
	UPROPERTY()
	TSubclassOf<class UPlayerWidget> WidgetBP_PlayerWidget;

	// ȭ�鿡 ����� �÷��̾� ���� ��ü
	UPROPERTY()
	class UPlayerWidget* PlayerWidget;

public :
	AGamePlayerController();

protected:
	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;

private :
	void OnGetItemPressed();
	void OnGetItemReleased();

	void OnFirePressed();
	void OnFireReleased();

	void OnReloadPressed();

	void OnZoomPressed();
	void OnZoomReleased();

	void ZoomIn();
	void ZoomOut();

	void OnJumpInput();

	void OnVerticalInput(float axis);
	void OnHorizontalInput(float axis);

	void OnMouseX(float axis);
	void OnMouseY(float axis);




public :
	// PlayerWidget ��ü�� ��ȯ�մϴ�.
	FORCEINLINE class UPlayerWidget* GetPlayerWidget() const
	{
		return PlayerWidget;
	}


	static FAuthType CheckAuthonication(AActor* actor, APlayerController* ownerClient);


	
};
