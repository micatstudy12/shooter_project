
#include "Actor/BulletActor/BulletActor.h"
#include "Actor/WorldItem/WorldItemActor.h"

#include "GameFramework/ProjectileMovementComponent.h"

#include "Components/SphereComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"

#include "Net/UnrealNetwork.h"

#include "../shooter_project.h"


ABulletActor::ABulletActor()
{
	PrimaryActorTick.bCanEverTick = true;

	BulletCollision = CreateDefaultSubobject<USphereComponent>(TEXT("BULLET_COLLISION"));
	SetRootComponent(BulletCollision);

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>("SM_BULLET");
	BulletMesh->SetupAttachment(GetRootComponent());


	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("PROJECTILE_MOVEMENT"));
	ProjectileMovement->bSweepCollision = true;
	
	bReplicates = true;
}

void ABulletActor::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle,
		FTimerDelegate::CreateUObject(this, &ThisClass::DestroyBullet),
		BULLETACTOR_LIFETIME, false);

	BulletMesh->SetCollisionProfileName(TEXT("NoCollision"));
	


	BulletCollision->OnComponentBeginOverlap.AddUniqueDynamic(
		this, &ThisClass::OnBulletOverllap);
}

void ABulletActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ThisClass, ProjectileMovement);
	DOREPLIFETIME(ThisClass, BulletCollision);
	DOREPLIFETIME(ThisClass, Direction);
}

void ABulletActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABulletActor::InitializeBullet(float damage, FVector direction)
{
	BulletDamage = damage;
	Direction = direction;
	bIsInitialized = true;

	UpdateDirection();
}

void ABulletActor::DestroyBullet()
{
	
	Destroy();
}

void ABulletActor::UpdateDirection_Implementation()
{
	ProjectileMovement->Velocity = Direction * ProjectileMovement->InitialSpeed;
}

void ABulletActor::OnBulletOverllap(
	UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (!bIsInitialized) return;

	if (Cast<ABulletActor>(OtherActor) != nullptr) return;
	if (Cast<AWorldItemActor>(OtherActor) != nullptr) return;

	bool isHitCharacterMesh = false;

	// ���� ���� ���
	if (Cast<USkeletalMeshComponent>(OtherComp))
	{
		UGameplayStatics::ApplyDamage(OtherActor, 
			BulletDamage, 
			nullptr, this, UDamageType::StaticClass());
		isHitCharacterMesh = true;
	}
	// �Ӹ��� ���� ���
	else if (Cast<USphereComponent>(OtherComp) && OtherComp->ComponentHasTag(TEXT("Head")))
	{
		UGameplayStatics::ApplyDamage(OtherActor, 
			BulletDamage * 10.0f, 
			nullptr, this, UDamageType::StaticClass());
		isHitCharacterMesh = true;
	}

	if (!isHitCharacterMesh && !OtherActor->ActorHasTag(TEXT("Character")))
	{
		isHitCharacterMesh = true;
	}

	if (isHitCharacterMesh)
	{
		if (TimerHandle.IsValid())
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle);

		DestroyBullet();
	}
}

