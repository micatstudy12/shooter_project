#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BulletActor.generated.h"

UCLASS()
class ABulletActor : public AActor
{
	GENERATED_BODY()

protected :
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	class UStaticMeshComponent * BulletMesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	class USphereComponent* BulletCollision;

private :
	float BulletDamage;
	FTimerHandle TimerHandle;

	UPROPERTY()
	bool bIsInitialized;

	UPROPERTY(Replicated)
	FVector Direction;


public:	
	ABulletActor();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void GetLifetimeReplicatedProps(
		TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Tick(float DeltaTime) override;

public :
	void InitializeBullet(float damage, FVector direction);

private :
	UFUNCTION()
	void DestroyBullet();

	UFUNCTION(Server, Reliable)
	void UpdateDirection();

	UFUNCTION()
	void OnBulletOverllap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, 
		const FHitResult& SweepResult);

};
