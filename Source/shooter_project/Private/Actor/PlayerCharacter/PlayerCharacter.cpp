#include "Actor/PlayerCharacter/PlayerCharacter.h"
#include "Actor/PlayerController/GamePlayerController.h"
#include "Actor/WorldItem/WorldItemActor.h"
#include "Actor/GunActor/GunActor.h"
#include "Actor/PlayerState/GamePlayerState.h"


#include "Component/PlayerCharacterMovementComponent/PlayerCharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Camera/CameraComponent.h"

#include "AnimInstance/PlayerCharacterAnimInstance/PlayerCharacterAnimInstance.h"

#include "Widget/PlayerWidget/PlayerWidget.h"
#include "Object/CameraShake/FireCameraShake.h"

#include "Struct/WorldItemInfo.h"

#include "../shooter_project.h"
#include "Net/UnrealNetwork.h"

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.SetDefaultSubobjectClass<UPlayerCharacterMovementComponent>(
		ACharacter::CharacterMovementComponentName))
	// ACharacter::CharacterMovementComponentName 로 
	// 생성된 컴포넌트(기본 CharacterMovementComponent)의 형식을
	// UPlayerCharacterMovementComponent 로 교체합니다.
{
	static ConstructorHelpers::FObjectFinder<UAnimMontage> ANIMMONTAGE_RELOAD(
		TEXT("/Script/Engine.AnimMontage'/Game/JetpackAnimSet/Animations/Standard/AnimMontage_Reload.AnimMontage_Reload'"));
	if (ANIMMONTAGE_RELOAD.Succeeded()) AnimMontage_Reload = ANIMMONTAGE_RELOAD.Object;

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_BODY(
		TEXT("/Script/Engine.SkeletalMesh'/Game/Characters/Mannequins/Meshes/SKM_Quinn.SKM_Quinn'"));

	if (SK_BODY.Succeeded()) GetMesh()->SetSkeletalMesh(SK_BODY.Object);

	static ConstructorHelpers::FClassFinder <UPlayerCharacterAnimInstance > ANIMBP_PLAYERCHARR(
		TEXT("/Script/Engine.AnimBlueprint'/Game/Blueprints/AnimInstance/AnimBP_PlayerCharacter.AnimBP_PlayerCharacter_C'"));

	if (ANIMBP_PLAYERCHARR.Succeeded()) GetMesh()->SetAnimClass(ANIMBP_PLAYERCHARR.Class);


	// 컨트롤러의 Yaw 회전을 사용하지 않습니다.
	bUseControllerRotationYaw = false;

	// 스프링암 컴포넌트 추가
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SPRING_ARM"));
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f));
	SpringArmComponent->SocketOffset = FVector::RightVector * 50.0f;

	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->bInheritPitch = true;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = true;

	HeadCollision = CreateDefaultSubobject<USphereComponent>(TEXT("HEAD_COLLISION"));
	HeadCollision->SetupAttachment(GetMesh(), TEXT("Socket_Head"));

	// 카메라 컴포넌트 추가
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CAM_COMP"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	PrimaryActorTick.bCanEverTick = true;

}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, CurrentSpeed);
	DOREPLIFETIME(ThisClass, InputAxisRaw);
	DOREPLIFETIME(ThisClass, EquippedGunActor);
	DOREPLIFETIME(ThisClass, EquippedItemType);
	DOREPLIFETIME(ThisClass, CameraPitchAngle);
	DOREPLIFETIME(ThisClass, CameraComponent);
	DOREPLIFETIME(ThisClass, EquippedWeaponInfo);
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetCapsuleComponent()->OnComponentBeginOverlap.AddUniqueDynamic(
		this, &APlayerCharacter::OnPlayerCharacterBeginOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddUniqueDynamic(
		this, &APlayerCharacter::OnPlayerCharacterEndOverlap);

	Cast<UPlayerCharacterAnimInstance>(GetMesh()->GetAnimInstance())->
		OnReleadedEvent.AddUObject(this, &ThisClass::OnReloaded);


	GetMesh()->SetCollisionProfileName(TEXT("PhysicsBody"));
	GetMesh()->SetGenerateOverlapEvents(true);


	Tags.Add(TEXT("Character"));
	HeadCollision->ComponentTags.Add(TEXT("Head"));


	AGamePlayerState * playerState = Cast<AGamePlayerState>(GetPlayerState());
	if (IsValid(playerState))
	{
		PLOG(TEXT("State::CurrentHp : %.2f"), playerState->GetCurrentHp());
	}

	bReplicates = true;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentSpeed = GetVelocity().Length();



	AGamePlayerController* playerController = Cast<AGamePlayerController>(GetController());
	//if (!HasAuthority() && GetController() == GetWorld()->GetFirstPlayerController())
	FAuthType authType = AGamePlayerController::CheckAuthonication(this, playerController);
	if (authType  == IS_SERVER || authType == IS_MYCLIENT)
	{
		UpdatePitchAngle();


		if (IsFireStarted)
		{
			Fire();
		}

		if (IsEquipped())
		{
			UpdateFireDirection();
		}
	}
	
	
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float APlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);


	AGamePlayerState* playerState = Cast<AGamePlayerState>(GetPlayerState());
	if (IsValid(playerState))
	{
		float currentHp = playerState->GetCurrentHp();
		currentHp -= DamageAmount;
		playerState->SetCurrentHp(currentHp);

		if (currentHp <= 0.0f)
		{
			PLOG(TEXT("Die"));
			return currentHp;
		}
	}

	return 0.0f;
}

void APlayerCharacter::OnPlayerCharacterBeginOverlap(
	UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	// 겹친 액터가 TAG_WORLDITEM 태그를 갖는 경우
	if (OtherActor->ActorHasTag(TAG_WORLDITEM))
	{
		// 배열에 겹친 WorldItem 추가
		AddOverlappedWorldItem(Cast<AWorldItemActor>(OtherActor));

		// 상호작용 위젯 표시
		ShowNearestWorldItemInteractionWidget();
	}
}

void APlayerCharacter::OnPlayerCharacterEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// 겹친 액터가 TAG_WORLDITEM 태그를 갖는 경우
	if (OtherActor->ActorHasTag(TAG_WORLDITEM))
	{
		// 배열에 겹침이 끝난 WorldItem 제거
		RemoveOverlappedWorldItem(Cast<AWorldItemActor>(OtherActor));

		// 상호작용 위젯 표시
		ShowNearestWorldItemInteractionWidget();
	}
}

void APlayerCharacter::AddOverlappedWorldItem(AWorldItemActor* worldItem)
{
	// 이미 추가된 액터인 경우 함수 호출 종료
	if (OverlappedWorldItems.Contains(worldItem)) return;

	// 배열에 추가
	OverlappedWorldItems.Add(worldItem);

	// 가까운 거리순으로 정렬합니다.
	SortWorldItemByDistance();
}

void APlayerCharacter::RemoveOverlappedWorldItem(AWorldItemActor* worldItem)
{
	// 추가되지 않은 액터인 경우 함수 호출 종료
	if (!OverlappedWorldItems.Contains(worldItem)) return;

	// 배열에서 제거
	OverlappedWorldItems.Remove(worldItem);

	// 가까운 거리순으로 정렬합니다.
	SortWorldItemByDistance();
}

void APlayerCharacter::SortWorldItemByDistance()
{
	if (OverlappedWorldItems.Num() < 2) return;

	// 캐릭터 위치를 얻습니다.
	FVector characterLocation = GetActorLocation();

	OverlappedWorldItems.Sort(
		[characterLocation](const AWorldItemActor& a, const AWorldItemActor& b)
		{
			FVector aLocation = a.GetActorLocation();
			float aDistance = FVector::Distance(characterLocation, aLocation);

			FVector bLocation = b.GetActorLocation();
			float bDistance = FVector::Distance(characterLocation, bLocation);

			return aDistance < bDistance;
		});
}

void APlayerCharacter::ShowNearestWorldItemInteractionWidget()
{
	// 자신의 플레이어 컨트롤러를 얻습니다.
	AGamePlayerController* playerController =
		Cast<AGamePlayerController>(GetController());

	if (!IsLocallyControlled()) return;
	if (!IsValid(playerController)) return;


	// 플레이어 위젯 객체를 얻습니다.
	UPlayerWidget* playerWidget = playerController->GetPlayerWidget();

	// 플레이어 위젯이 유효하지 않은 경우 함수 호출 종료
	//if (!IsValid(playerWidget)) return;



	if (OverlappedWorldItems.Num() < 1)
	{
		// 위젯 숨기기
		playerWidget->HideInteractionWidget();
		return;
	}

	// 가장 가까운 월드 아이템 액터를 얻습니다.
	AWorldItemActor* nearestWorldItemActor = OverlappedWorldItems[0];

	// 아이템 정보를 얻습니다.
	FWorldItemInfo* nearestWorldItemInfo = nearestWorldItemActor->GetWorldItemInfo();

	// 아이템 정보를 얻지 못한 경우
	if (nearestWorldItemInfo == nullptr)
	{
		// 위젯 숨기기
		playerWidget->HideInteractionWidget();
		return;
	}

	// 아이템 이름을 얻습니다.
	FText nearestWorldItemName = nearestWorldItemInfo->ItemName;

	// 위젯을 표시합니다.
	playerWidget->ShowInteractionWidget(nearestWorldItemName);

}

void APlayerCharacter::EquipItem_Implementation()
{

	AGamePlayerController* playerController = Cast<AGamePlayerController>(GetController());
	//if (!HasAuthority() && GetController() == GetWorld()->GetFirstPlayerController())
	FAuthType authType = AGamePlayerController::CheckAuthonication(this, playerController);
	if (authType == IS_SERVER || authType == IS_MYCLIENT)
	{

		if (IsEquipped())
		{
			EquippedGunActor->Destroy();
			EquippedGunActor = nullptr;
		}

		EquipWeapon();
	}

}

void APlayerCharacter::EquipWeapon_Implementation()
{
	EquippedGunActor = GetWorld()->SpawnActor<AGunActor>(EquippedWeaponInfo.GunActorClass);

	EquippedGunActor->InitializeGunActor(&EquippedWeaponInfo);
	EquippedGunActor->SetOwner(this);
	EquippedGunActor->SetOwnerCharacter(this);
	OnFireFinished(EquippedGunActor->GetMaxBulletCount(), EquippedGunActor->GetMaxBulletCount());


	FName socketName;
	switch (EquippedWeaponInfo.ItemType)
	{
	case EWorldItemType::Weapon_Pistol:
		socketName = TEXT("Socket_Pistol");
		break;
	case EWorldItemType::Weapon_Rifle:
		socketName = TEXT("Socket_Rifle");
		break;
	case EWorldItemType::Weapon_Shotgun:
		socketName = TEXT("Socket_Shotgun");
		break;
	}

	// 생성된 총 액터를 캐릭터 Mesh 에 붙입니다.
	EquippedGunActor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	EquippedGunActor->AttachToComponent(GetMesh(),
		FAttachmentTransformRules::KeepWorldTransform, socketName);
	EquippedGunActor->SetActorRelativeLocation(FVector::ZeroVector);
	EquippedGunActor->SetActorRelativeRotation(FVector::RightVector.Rotation());

	EquippedGunActor->OnFireFinished.AddUObject(this, &ThisClass::OnFireFinished);


	EquippedItemType = EquippedWeaponInfo.ItemType;
}

void APlayerCharacter::OnFireFinished(int32 remain, int32 max)
{
	// 자신의 플레이어 컨트롤러를 얻습니다.
	AGamePlayerController* playerController =
		Cast<AGamePlayerController>(GetController());

	if (!IsLocallyControlled()) return;
	if (!IsValid(playerController)) return;

	playerController->GetPlayerWidget()->UpdateBulletRemainText(remain, max);
}

void APlayerCharacter::Fire_Implementation()
{
	if (!IsValid(EquippedGunActor)) return;
	EquippedGunActor->Fire();

	if (GetController() == GetWorld()->GetFirstPlayerController())
	{
		Cast<AGamePlayerController>(GetController())->
			ClientStartCameraShake(UFireCameraShake::StaticClass());
	}

}

void APlayerCharacter::OnReloaded()
{
	if (!IsValid(EquippedGunActor)) return;

	EquippedGunActor->OnReloaded();
	EquippedGunActor->FinishReload();



	// 자신의 플레이어 컨트롤러를 얻습니다.
	AGamePlayerController* playerController =
		Cast<AGamePlayerController>(GetController());

	if (!IsLocallyControlled()) return;
	if (!IsValid(playerController)) return;

	playerController->GetPlayerWidget()->UpdateBulletRemainText(
		EquippedGunActor->GetRemainBullets(),
		EquippedGunActor->GetMaxBullets());



}

void APlayerCharacter::UpdatePitchAngle_Implementation()
{
	AGamePlayerController* playerController = Cast<AGamePlayerController>(
		GetController());

	if (!IsValid(playerController)) return;

	CameraPitchAngle = playerController->GetControlRotation().Pitch;
	
}

void APlayerCharacter::PickupItem_Implementation()
{
	// 주울 아이템이 없다면 함수 호출 종료;
	if (OverlappedWorldItems.Num() == 0) return;

	// 아이템 액터
	AWorldItemActor* worldItemActor = OverlappedWorldItems[0];

	// 아이템 정보
	FWorldItemInfo* worldItemInfo = worldItemActor->GetWorldItemInfo();

	// 무기 아이템인 경우
	bool isWeapon = EWorldItemType::RANGE_WEAPON_START < worldItemInfo->ItemType &&
		worldItemInfo->ItemType < EWorldItemType::RANGE_WEAPON_END;

	if (isWeapon) {
		EquippedWeaponInfo = *worldItemInfo;
		//EquipWeapon();
		EquipItem();
	}
}

void APlayerCharacter::UpdateFireDirection_Implementation()
{
	AGamePlayerController* playerController = Cast<AGamePlayerController>(
		GetController());

	if (!IsValid(playerController)) return;

	EquippedGunActor->UpdateFireDirection(
		CameraComponent->GetComponentLocation(),
		Cast<AActor>(playerController)->GetActorForwardVector());
}

void APlayerCharacter::OnFirePressed()
{
	if (!IsEquipped()) return;

	IsFireStarted = true;
}

void APlayerCharacter::OnFireReleased()
{
	IsFireStarted = false;
}

void APlayerCharacter::OnGetItemPressed()
{
	PickupItem();
}

void APlayerCharacter::OnReloadPressed()
{
	WLOG(TEXT("EquippedGunActor is not Valid = %d"), !IsValid(EquippedGunActor));
	if (!IsValid(EquippedGunActor)) return;

	WLOG(TEXT("EquippedGunActor is not Reloadable = %d"), (!EquippedGunActor->IsReloadable()));
	if (!EquippedGunActor->IsReloadable()) return;

	WLOG(TEXT("EquippedGunActor->IsReloading() = %d"), EquippedGunActor->IsReloading());
	if (EquippedGunActor->IsReloading()) return;

	EquippedGunActor->StartReload();

	FName montageSection = FName();
	switch (EquippedItemType)
	{
	case EWorldItemType::Weapon_Pistol:
		montageSection = MONTAGESECTION_PISTOL_RELOAD;
		break;

	case EWorldItemType::Weapon_Rifle:
		montageSection = MONTAGESECTION_RIFLE_RELOAD;
		break;

	case EWorldItemType::Weapon_Shotgun:
		montageSection = MONTAGESECTION_SHOTGUN_RELOAD;
		break;
	}

	PlayAnimMontage(AnimMontage_Reload, 1.0f, montageSection);
}

void APlayerCharacter::OnJumpInput()
{
	Jump();
}

void APlayerCharacter::OnHorizontalInput(float axis)
{
	UPlayerCharacterMovementComponent* movementComponent =
		Cast<UPlayerCharacterMovementComponent>(GetCharacterMovement());

	InputAxisRaw.Y = axis;

	movementComponent->OnHorizontalMovement(axis);
}

void APlayerCharacter::OnVerticalInput(float axis)
{
	UPlayerCharacterMovementComponent* movementComponent =
		Cast<UPlayerCharacterMovementComponent>(GetCharacterMovement());

	InputAxisRaw.X = axis;

	movementComponent->OnVerticalMovement(axis);
}

void APlayerCharacter::ZoomIn()
{
	CameraComponent->FieldOfView = 45.0f;
}

void APlayerCharacter::ZoomOut()
{
	CameraComponent->FieldOfView = 90.0f;
}

bool APlayerCharacter::IsEquipped() const
{
	return IsValid(EquippedGunActor);
}

