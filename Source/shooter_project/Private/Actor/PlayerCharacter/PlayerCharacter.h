#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enum/WorldItemType.h"


#include "Struct/WorldItemInfo.h"

#include "PlayerCharacter.generated.h"

#define MONTAGESECTION_PISTOL_RELOAD		TEXT("Section_Pistol")
#define MONTAGESECTION_SHOTGUN_RELOAD		TEXT("Section_ShotGun")
#define MONTAGESECTION_RIFLE_RELOAD			TEXT("Section_Rifle")

UCLASS()
class APlayerCharacter : public ACharacter
{
	GENERATED_BODY()


protected :
	UPROPERTY(VisibleAnywhere)
	class UAnimMontage* AnimMontage_Reload;

	UPROPERTY(VisibleAnywhere)
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, Replicated)
	class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere)
	class USphereComponent* HeadCollision;


	// 입력 축 값을 나타냅니다.
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated)
	FIntVector InputAxisRaw;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated)
	float CurrentSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated)
	EWorldItemType EquippedItemType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool IsFireStarted;


private :
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Replicated)
	class AGunActor* EquippedGunActor;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Replicated)
	float CameraPitchAngle;

	// 장착중인 무기 정보
	UPROPERTY(Replicated)
	FWorldItemInfo EquippedWeaponInfo;




private :
	// 겹친 World Item 액터들을 나타냅니다.
	UPROPERTY()
	TArray<class AWorldItemActor*> OverlappedWorldItems;


public:
	APlayerCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(
		TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(
		float DamageAmount,
		struct FDamageEvent const& DamageEvent,
		class AController* EventInstigator,
		AActor* DamageCauser) override;


private :
	UFUNCTION()
	void OnPlayerCharacterBeginOverlap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnPlayerCharacterEndOverlap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex);

	// 겹친 WorldItem 액터를 추가합니다.
	void AddOverlappedWorldItem(class AWorldItemActor* worldItem);

	// 겹쳤던 WorldItem 액터를 제거합니다.
	void RemoveOverlappedWorldItem(class AWorldItemActor* worldItem);

	// 거리를 기준으로 가까운 순서대로 WorldItem 들을 정렬합니다.
	void SortWorldItemByDistance();

	// 가장 가까운 월드 아이템의 상호작용 위젯을 표시합니다.
	void ShowNearestWorldItemInteractionWidget();

	// 아이템을 장착합니다.
	// worldItemInfo : 장착 가능한 월드 아이템 정보를 전달합니다.
	UFUNCTION(NetMulticast, Reliable)
	void EquipItem();

	UFUNCTION(NetMulticast, Reliable)
	void EquipWeapon();

	UFUNCTION()
	void OnFireFinished(int32 remain, int32 max);


	UFUNCTION(Server, unreliable)
	void Fire();

	UFUNCTION()
	void OnReloaded();

private :
	UFUNCTION(Server, Reliable)
	void UpdatePitchAngle();

	UFUNCTION(Server, Reliable)
	void PickupItem();

	UFUNCTION(Server, Reliable)
	void UpdateFireDirection();


public :
	void OnFirePressed();
	void OnFireReleased();

	void OnGetItemPressed();

	void OnReloadPressed();

	void OnJumpInput();

	void OnHorizontalInput(float axis);
	void OnVerticalInput(float axis);

	void ZoomIn();
	void ZoomOut();

	FORCEINLINE FIntVector GetInputAxisRaw() const
	{
		return InputAxisRaw;
	}

	FORCEINLINE float GetCurrentSpeed() const
	{
		return CurrentSpeed;
	}

	FORCEINLINE EWorldItemType GetEquippedItemType()
	{
		return EquippedItemType;
	}

	FORCEINLINE float GetCameraPitchAngle() const
	{
		return CameraPitchAngle;
	}

	bool IsEquipped() const;
};
