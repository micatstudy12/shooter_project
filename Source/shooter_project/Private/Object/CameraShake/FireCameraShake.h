// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LegacyCameraShake.h"
#include "FireCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class UFireCameraShake : public ULegacyCameraShake
{
	GENERATED_BODY()

public :
	UFireCameraShake();
	
};
