// Fill out your copyright notice in the Description page of Project Settings.


#include "Object/CameraShake/FireCameraShake.h"

UFireCameraShake::UFireCameraShake()
{
	bSingleInstance = true;
	OscillationDuration = 0.2f;

	// ���� ����
	RotOscillation.Pitch.Amplitude = 0.5f;

	// ���� �ӵ�
	RotOscillation.Pitch.Frequency = 10.0;

	RotOscillation.Yaw.Amplitude = 0.5f;
	RotOscillation.Yaw.Frequency = 10.0;
}