#pragma once

#include "CoreMinimal.h"
#include "WorldItemType.generated.h"

/**
 * 월드 아이템 타입을 나타내기 위하여 사용되는 열거 형식입니다.
 */
UENUM(BlueprintType)
enum class EWorldItemType : uint8
{
	RANGE_WEAPON_START = 0,

	Weapon_Pistol = 1			UMETA(Displayname = "Pistol"),
	Weapon_Rifle = 2			UMETA(Displayname = "Rifle"),
	Weapon_Shotgun = 3			UMETA(Displayname = "Shotgun"),

	RANGE_WEAPON_END = 4,

	//RANGE_CONSUME_START,
	//
	//Consume_HealHp,
	//Consume_HealStemina,
	//
	//RANGE_CONSUME_END,
};
