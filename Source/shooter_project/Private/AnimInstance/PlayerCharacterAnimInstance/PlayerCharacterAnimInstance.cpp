#include "AnimInstance/PlayerCharacterAnimInstance/PlayerCharacterAnimInstance.h"

#include "Actor/PlayerCharacter/PlayerCharacter.h"
#include "Component/PlayerCharacterMovementComponent/PlayerCharacterMovementComponent.h"

#include "Net/UnrealNetwork.h"
#include "../shooter_project.h"



void UPlayerCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	APlayerCharacter* owner = Cast<APlayerCharacter>(TryGetPawnOwner());
	

	if (!IsValid(owner)) return;

	//Speed = owner->GetVelocity().Length();
	Speed = owner->GetCurrentSpeed();
	IsMove = Speed > 1.0f;

	Direction = FVector(owner->GetInputAxisRaw());

	IsInAir = owner->GetMovementComponent()->IsFalling();
	ZVelocity = IsInAir ? owner->GetVelocity().Z : 0.0f;
	PitchAngle = owner->GetCameraPitchAngle();

	// 아래를 바라보고 있는 경우 처리
	if (PitchAngle - 180.0f > 0.0f) PitchAngle -= 360.0f;

	IsEquipped = owner->IsEquipped();

	if (IsEquipped)
	{
		EquippedItemType = owner->GetEquippedItemType();
		//PLOG(TEXT("EquippedItemType = %d"), EquippedItemType);
	}

}

void UPlayerCharacterAnimInstance::AnimNotify_OnReloaded()
{
	OnReleadedEvent.Broadcast();
}
